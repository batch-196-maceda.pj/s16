console.log("hello");
/*
	1. Debug the following code to return the correct value and mimic the output.
*/

	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	let sum=num1+num2;
	console.log(sum);
	



	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	let sum1=num3+num4;
	console.log(sum1);
	
	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	let difference =num5-num6;
	console.log(difference);
		
/*
w
	2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.
   (Expected output: 525600)

*/
	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;


	
	let resultMinutes= minutesHour*hoursDay*daysYear;
	console.log("There are "+resultMinutes+" minutes in one year");
	

/*
	3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
    (hint: Multiply the °C temperature by 1.8. Add 32 to this number. This is the answer in °F.)
*/
	let tempCelsius = 132;
	let tempFareheight=tempCelsius*1.8+32;
	console.log("The temp of 132 ceslcius in Farenheight is "+ tempFareheight+"°F");

/*
	4a. Given the values below, identify if the values of the following variable are divisible by 8.
	   -Use a modulo operator to identify the divisibility of the number to 8.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
	   -Log a message in the console if num7 is divisible by 8.
	   -Log the value of isDivisibleBy8 in the console.

*/

	let num7 = 165;
	let num9 = 8;
	let remainder=num7%num9;

	console.log("The remainder of num7 is "+remainder);

	//Log the value of the remainder in the console.
	console.log("Is num7 divisible by 8?"+ (remainder===0));
	//Log the value of isDivisibleBy8 in the console.


/*
	4b. Given the values below, identify if the values of the following variable are divisible by 4.
	   -Use a modulo operator to identify the divisibility of the number to 4.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
	   -Log a message in the console if num8 is divisible by 4.
	   -Log the value of isDivisibleBy4 in the console.

*/
	let num8 = 348;
	let num10=4;
	let remainder1=num8%num10;
	console.log("The remainder of num8 is "+remainder1)
	//Log the value of the remainder in the console.

	let isDivisibleBy4=4
	console.log("Is num8 divisible by 4? "+(remainder1===0));
	//Log the value of isDivisibleBy4 in the console.